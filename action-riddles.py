#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import random

from assistant import Assistant, read_json_file

# ======================================================================================================================

assist = Assistant()
riddles = None
file_path = os.path.dirname(os.path.realpath(__file__)) + "/"


# ======================================================================================================================

def load_riddles(lang):
    """ Read all riddles from file """

    path = file_path + "riddles/" + lang.lower() + ".json"
    riddles = read_json_file(path)
    random.shuffle(riddles)
    return riddles


# ======================================================================================================================

def get_new_riddle():
    """ Get first entry and append it to the end -> no repetition until every entry selected """

    riddle = riddles.pop(0)
    riddles.append(riddle)
    return riddle


# ======================================================================================================================

def check_current_riddle():
    """ Get current riddle id from status file """

    path = file_path + "/skilldata/status.txt"
    if (os.path.isfile(path)):
        with open(path, "r") as file:
            line = next(file)
            rid = int(line)
    else:
        rid = 0
    return rid


# ======================================================================================================================

def write_current_riddle(riddle_id):
    """ Write riddle id in status file """

    path = file_path + "/skilldata/status.txt"
    with open(path, "w+") as file:
        file.write(str(riddle_id))


# ======================================================================================================================

def callback_get_riddle(hermes, intent_message):
    """ Read new riddle or repeat open riddle """

    riddle_id = check_current_riddle()

    if (riddle_id == 0):
        riddle = get_new_riddle()
        write_current_riddle(riddle["id"])
        result_sentence = riddle["question"]
    else:
        riddle = [r for r in riddles if r["id"] == riddle_id][0]
        result_sentence = riddle["question"]

    hermes.publish_end_session(intent_message.session_id, result_sentence)


# ======================================================================================================================

def callback_check_riddle(hermes, intent_message):
    """ Check if the solution of the user is correct """

    riddle_id = check_current_riddle()

    if (riddle_id == 0):
        result_sentence = random.choice(assist.get_talks()["no_riddle"])
    else:
        if (intent_message.slots is not None and len(intent_message.slots.solution) >= 1):
            riddle = [r for r in riddles if r["id"] == riddle_id][0]
            solution = str(intent_message.slots.solution.first().value)

            if (solution.lower() == riddle["answer"].lower()):
                write_current_riddle(0)
                result_sentence = assist.get_text("correct_solution")
            else:
                result_sentence = assist.get_text("wrong_solution").format(solution)
        else:
            result_sentence = assist.get_text("not_understood")

    hermes.publish_end_session(intent_message.session_id, result_sentence)


# ======================================================================================================================

riddles = load_riddles(assist.get_config()["secret"]["language"])

if __name__ == "__main__":
    h = assist.get_hermes()
    h.connect()
    h.subscribe_intent("DANBER:getRiddle", callback_get_riddle)
    h.subscribe_intent("DANBER:checkRiddle", callback_check_riddle)
    h.loop_forever()
